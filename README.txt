CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Video Tutorial
 * Maintainers


INTRODUCTION
------------

Permission Matrix module to customize your own Permission matrix page.


CONFIGURATION
-------------

/admin/config/configure-permission-matrix
 * Select the Permissions and change the permission label if require.

/admin/config/permission_group
 * Create Permission groups to bind multiple permissions in single set of permission.

/admin/config/permission-matrix
 * Customized Permission matrix to assign permissions to roles.

Video Tutorial
--------------

https://www.youtube.com/watch?v=ADCFeIptBtc


MAINTAINERS
-----------

Current maintainers:
 * Praveen Paliya (ppaliya) - https://www.drupal.org/u/ppaliya
 * Surya Prakash Gangwar (gangwarsurya) - https://www.drupal.org/u/gangwarsurya


This project has been sponsored by:
 * Xeliumtech Solutions Pvt. Ltd.
   Founded in 2010, XeliumTech Solutions, a member of Nasscom, Started in 2010 
   with a vision to be a quality one-stop solutions company. Today, we are 
   proud to be a end to end trusted Software services partners for companies 
   globally across North America, Australia, UK,  Europe, Middle-East, 
   South-East Asia. Visit: https://www.drupal.org/xeliumtech-solutions-pvt-ltd 
   for more information.
