<?php

namespace Drupal\permission_matrix\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Permission group entities.
 */
interface PermissionGroupInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
